# Onchain template for generative.xyz

This template uses `vite` to build and bundle the project in one file `index.html` under `dist` folder.


## Preparation


Clone the template

```
git clone https://gitlab.com/maxgeneris/onchain-template.git my-project-name

```

Install the dependencies

```bash
npm install
```


## Basic commands

Starts a development server

```bash
npm start
```

There are a few Key bindings. Type 's' to save the svg file; Type 'x' to save a high resolution png.


Compile TypeScript files and bundle the resulting javascript into `index.html`
```bash
npm run build
``` 





