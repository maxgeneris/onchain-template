type Tupple4<T> = [T, T, T, T];
export class Color {
  private _rgba: Tupple4<number>;
  private _hsla: Tupple4<number>;

  /**
   * Creates a color object from `r`, `g`, `b` and `a` components in the range of 0 to 255.
   * @param r - The *red* component.
   * @param g - The *green* component.
   * @param b - The *blue* component.
   * @param a - The *alpha* component.
   */
  constructor(r: number, g: number, b: number, a: number = 255) {
    this._rgba = [r / 255, g / 255, b / 255, a / 255];
    this._hsla = rgbToHsl(this._rgba);
  }
  private set hsla(ar: Tupple4<number>) {
    this._hsla = ar;
    this._rgba = hslToRgb(...this._hsla);
  }
  private get hsla() {
    return [...this._hsla];
  }
  public get h() {
    return this._hsla[0];
  }
  public get t() {
    return rgb2temp(...this._rgba);
  }
  public get s() {
    return this._hsla[1];
  }
  public get l() {
    return this._hsla[2];
  }
  public get a() {
    return this._hsla[3];
  }
  sat(s: number) {
    const nc = new Color(0, 0, 0);
    nc._hsla = [...this.hsla];
    nc._hsla[1] = s;
    return nc;
  }
  hue(h: number) {
    const nc = new Color(0, 0, 0);
    nc._hsla = [...this.hsla];
    nc._hsla[0] = h;
    return nc;
  }
  lum(l: number) {
    const nc = new Color(0, 0, 0);
    nc._hsla = [...this.hsla];
    nc._hsla[2] = l;
    return nc;
  }
  alpha(a: number) {
    const nc = new Color(0, 0, 0);
    nc._hsla = [...this.hsla];
    nc._hsla[3] = a;
    return nc;
  }

  toString() {
    return this.a < 1
      ? `hsl(${this.h * 360}deg ${this.s * 100}% ${this.l * 100}% / ${
          this.a * 100
        }%)`
      : `hsl(${this.h * 360}deg ${this.s * 100}% ${this.l * 100}%)`;
  }
  /**
   * Parse a color string in the format #abc, #abcd, #aabbcc or #aabbccdd and returns a `Color` object.
   * @param rgba - The string to parse.
   * @returns A new `Color` object component.
   */
  static from(rgba: Color | string | number) {
    if (rgba instanceof Color) return rgba;

    if (typeof rgba === "number") {
      return new Color(255, 0, 0);
    } else {
      let m;
      switch (rgba.length) {
        case 9:
          m = rgba.match(
            /^[#](?<r>[0-9a-f]{2})(?<g>[0-9a-f]{2})(?<b>[0-9a-f]{2})(?<a>[0-9a-f]{2})$/i
          );

          if (m && m.groups) {
            if (m.groups)
              return new Color(
                parseInt(m.groups.r, 16),
                parseInt(m.groups.g, 16),
                parseInt(m.groups.b, 16),
                parseInt(m.groups.a, 16)
              );
            else throw new Error(`Error parsing color string '${rgba}'`);
          }
          break;
        case 7:
          m = rgba.match(
            /^[#](?<r>[0-9a-f]{2})(?<g>[0-9a-f]{2})(?<b>[0-9a-f]{2})$/i
          );

          if (m && m.groups) {
            if (m.groups)
              return new Color(
                parseInt(m.groups.r, 16),
                parseInt(m.groups.g, 16),
                parseInt(m.groups.b, 16)
              );
            else throw new Error(`Error parsing color string '${rgba}'`);
          }
          break;
        case 5:
          m = rgba.match(
            /^[#](?<r>[0-9a-f])(?<g>[0-9a-f])(?<b>[0-9a-f])(?<a>[0-9a-f])$/i
          );

          if (m && m.groups) {
            if (m.groups)
              return new Color(
                parseInt("" + m.groups.r + m.groups.r, 16),
                parseInt("" + m.groups.g + m.groups.g, 16),
                parseInt("" + m.groups.b + m.groups.b, 16),
                parseInt("" + m.groups.a + m.groups.a, 16)
              );
            else throw new Error(`Error parsing color string '${rgba}'`);
          }
          break;
        case 4:
          m = rgba.match(/^[#](?<r>[0-9a-f])(?<g>[0-9a-f])(?<b>[0-9a-f])$/i);

          if (m && m.groups) {
            if (m.groups)
              return new Color(
                parseInt("" + m.groups.r + m.groups.r, 16),
                parseInt("" + m.groups.g + m.groups.g, 16),
                parseInt("" + m.groups.b + m.groups.b, 16)
              );
            else throw new Error(`Error parsing color string '${rgba}'`);
          }
          break;
        default:
          throw new Error(`Error parsing color string '${rgba}'`);
      }
    }
  }
}

function rgbToHsl([r, g, b, a]: Tupple4<number>): Tupple4<number> {
  // Convert RGB values to percentages (0-1 range)

  // Find the maximum and minimum color values
  const max = Math.max(r, g, b);
  const min = Math.min(r, g, b);

  // Calculate the hue
  let h: number;
  if (max === min) {
    h = 0; // achromatic
  } else {
    const delta = max - min;
    if (max === r) {
      h = ((g - b) / delta) % 6;
    } else if (max === g) {
      h = (b - r) / delta + 2;
    } else {
      h = (r - g) / delta + 4;
    }
    h *= 60;
    if (h < 0) {
      h += 360;
    }
  }

  // Calculate the lightness and saturation
  const delta = max - min;

  const l = (max + min) / 2;
  const s = delta === 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));

  // Convert saturation and lightness to percentages (0-100 range)
  return [h / 360, s, l, a];
}

const hue2rgb = (p: number, q: number, t: number): number => {
  if (t < 0) t += 1;
  if (t > 1) t -= 1;
  if (t < 1 / 6) return p + (q - p) * 6 * t;
  if (t < 1 / 2) return q;
  if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
  return p;
};

function hslToRgb(h: number, s: number, l: number, a:number):Tupple4<number> {
  let r: number, g: number, b: number;

  if (s === 0) {
    r = g = b = l; // achromatic
  } else {
    const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    const p = 2 * l - q;
    r = hue2rgb(p, q, h + 1 / 3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1 / 3);
  }

  return [r,g,b,a];
}

function rgb2temp(red:number, green:number, blue:number, a:number):number {
    // Convert RGB values to linear color space
    const rLinear = red ;
    const gLinear = green ;
    const bLinear = blue ;
  
    // Calculate the inverse gamma correction
    const r = rLinear <= 0.04045 ? rLinear / 12.92 : Math.pow((rLinear + 0.055) / 1.055, 2.4);
    const g = gLinear <= 0.04045 ? gLinear / 12.92 : Math.pow((gLinear + 0.055) / 1.055, 2.4);
    const b = bLinear <= 0.04045 ? bLinear / 12.92 : Math.pow((bLinear + 0.055) / 1.055, 2.4);
  
    let temp = 0;

    if (r === g && g === b) {
      // A neutral color (gray)
      temp = 6500;
    } else {
      const x = (0.33267 - r * 0.16658 - g * 0.20228 + b * 0.35827) / (r * 0.7328 + g * 0.4296 + b * 0.3419);
      temp = 449 * Math.pow(x, -3) + 3525 * Math.pow(x, -2) + 6823.3 * x + 5520.33;
    }
  
    return Math.round(temp * 100) / 100; // round to 2 decimal places
  }
  