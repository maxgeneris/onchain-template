import { Color } from "./lib/color";

declare const sfc32:any, cyrb128:any, seed:string;

// Math functions
const {sin, cos, abs, PI, acos, asin, sqrt, round, floor} = Math, TAU = 2*PI, HALF_PI=PI/2; 

type Vec = [number, number];


// Vector functions  
const add = (a:Vec, b:Vec):Vec=> [a[0]+b[0], a[1]+b[1]];
const mulN = (a:Vec, b:number):Vec=> [a[0]*b, a[1]*b];
const mag = (a:Vec):number=> sqrt( a[0]**2+a[1]**2);
const addmN = (a:Vec, b:Vec, n:number):Vec=> [a[0]+b[0]*n, a[1]+b[1]*n];
const norm = (p:Vec, n:number=1):Vec => {
    const l = mag(p);
    return l ===0? [0,0] : mulN(p, n/l);
}  

const fieldGen = (a:number, b:number, o:number = 2) => (x:number, y:number) => {
    let v = 0;
    let g = 1;
    let f = 1;
    for(let i =0;i<o;i++) {
        v += g*sin(sin(x/a*f) + cos(y/b*f));
        f *=1.97834;
        g *=0.5;
    }
    return v;
};

const grad = (fld:(x:number, y:number)=>number, [x,y]:Vec):Vec => {
    const e = 1e-4;
    const dx1 = fld(x+e, y);
    const dx2 = fld(x-e, y);
    const dy1 = fld(x, y+e);
    const dy2 = fld(x, y-e);
    return [(dx1-dx2)/(2*e),(dy1 - dy2)/(2*e)];
}
const curl = (fld:(x:number, y:number)=>number, p:Vec):Vec => {
    const [a,b] = grad(fld, p);
    return [b, -a];
}
/**
 * Reverse the array elements
 * @param arr 
 * @returns a reverse copy of the array
 */
const reverse = (arr:any[]) => Array.from(arr, ((_,i)=> arr[arr.length-1-i]))

const svgD = ([m,...pts]:Vec[], closed:boolean=false) => {
    let d =  `M ${m.join(' ')}`;
    d += pts.map(p=>`L ${p.join(" ")}`).join(" ");
    d += closed? 'z':'';
    return d;
}

/**
 * Utility class to generate random values
 */

class Random {
    private prng:()=>number;
    constructor(seed) {
        if ( typeof cyrb128 === "function") this.prng = sfc32(...cyrb128(seed));

        else throw new Error("Failed to initialize Random class.");
    }

    /**
     * 
     * @returns Random float between [0 - 1)
     */
    unit () {
        return this.prng();
    }

    /**
     * 
     * @param p - probability
     * @returns `true` with the probability `p` `false` otherwise
     */
    prob (p:number) {
        return this.prng()<p ;
    } 

    /**
     * Return a `float` number in a range [m - M)
     * @param m - the lower end of the range
     * @param M - the higher end of the range
     * @returns - `float` in the [m - M) interval
     */
    float(m:number, M:number) {
        return this.prng()* (M-m) + m;
    }

    /**
     * Return an `int` number in a range [m - M)
     * @param m - the lower end of the range
     * @param M - the higher end of the range
     * @returns - `int` in the [m - M) interval
     */
    int(m:number, M:number) {
        return Math.floor(this.prng()* (M-m) + m);
    }

    /**
     * Return a random element of an array 
     * @param arr - input array from which to return a random value
     * @returns a random element from the input array 
     */
    pick(arr:any[]) {
        return arr[this.int(0, arr.length)];
    }
}
export const Palettes = {

    "pal0":["#DEC78E", "#192220", "#BE3C39","#7E2925","#CCB477","#647657","#2D4C40","#6AB29B",],
    "pal1": ["#B4BCA4", "#0E525A","#C2502E","#D47651","#0C353B","#F5AF6B","#897563","#6A402B",],
    "pal2": ["#F1D8AF","#785C74","#323666","#298D98","#E36075","#CF9A94","#5BE6CA","#F5BA6A"],
    "pal3":["#6D9995","#516F6B","#233C45","#EDDC8D","#E69837","#7ED0D6","#A89E7A","#3E371D"], 

}

interface Traits {
    Palette: string;
}


const svgns = "http://www.w3.org/2000/svg", xlinkns = "http://www.w3.org/1999/xlink";   

export const generate = (parent:HTMLElement) => {
    const R = new Random(seed);


    const features:Traits =  {
        "Palette": R.pick( Object.keys(Palettes)),
    }

    const [bg, accent, ...fg] = Palettes[features.Palette];
    const palette = Palettes[features.Palette];

    const ar = 0.67, W = innerWidth/innerHeight > ar ? ( innerHeight - 10)*ar : (innerWidth-10), H = W/ar ;
    const DW = 600, DH = DW/ar, HDW=DW/2, HDH = DH/2;


    const vg = (pid:string, tag:string, att:Object, bf?:HTMLElement|SVGElement) => {
        const p = document.getElementById(pid),
            e = document.createElementNS(svgns, tag);
        Object.keys(att).forEach(k => (k=== "xlink:href"? e.setAttributeNS(xlinkns, k, att[k]):e.setAttribute(k, att[k]) ))
        p && (undefined===bf? p.appendChild(e): p.insertBefore(e, bf));
        return e;
    }



    const svg = document.createElementNS(svgns, "svg"),
            sc = document.createElement("div");
    
    sc.setAttribute("id", "svgC");
    parent.style.background = "#ddd";
    parent.style.width = `${innerWidth-10}px`;
    parent.style.height = `${innerHeight-10}px`;
    parent.style.padding = `5px`;

    sc.style.width = `${W}px`;
    sc.style.height = `${H}px`;
    sc.style.margin = "0 auto";
    sc.style.boxShadow = "2px 3px 6px rgba(0,0,0,0.5)";
    sc.style.borderRadius = "0.5vh";

    parent.appendChild(sc);
    vg("svgC", "svg", {
        id: "art",
        xmlns: svgns,
        "xmlns:xlink": xlinkns,
        "shape-rendering": "geometricPrecision",
        width: DW,
        height: DH,
        viewBox: `${-HDW} ${-HDH} ${DW} ${DH} `, 
        style:`display:block; width:${W}; height:${H}`
    });
    vg("art", "defs", {
        id:"df",
    });

    vg("df", "filter", {
        id:"blur",
    });

    vg("blur", "feGaussianBlur", {
        "stdDeviation":R.int(2, 6)
    });

    vg("df", "filter", {id:"colorF"});
    vg("colorF", "feColorMatrix", {
        type:"matrix",
        values:`${R.float(0.2, 0.8)} 1 0 0 0
        0 0.7 0 0 0
        0 0 0.5 0 0
        0 0 0 1.1 0`
    })



    vg("art", "rect", {
        x:-HDW,
        y:-HDH,
        width: DW,
        height: DH,
        fill:bg,
        id:"bg"
    });
    vg("art", "g", {id:"g-blur", filter:"url(#blur)"});
    vg("art", "g", {id:"stripes"});

    for(let i = -200; i<200;i+=20) {
        let p:Vec = [i, R.float(-HDH, HDH)];
        
        const group = R.pick(["g-blur", "stripes"]);
        const d = svgD([p, add(p, [17, 0]), add(p, [17, -2*p[1]]), add(p, [0, -2*p[1]])], true);
        vg(group, "path", {
            d,
            fill:R.pick(palette),

            stroke: R.pick(palette)
        });
        

    }

    vg("art", "g",{
        id:"border",
        filter:"url(#colorF)"
    })
    vg("border", "rect", {
        x:-HDW,
        y:-HDH,
        width: DW,
        height:10,
        fill:bg,
        "fill-opacity":0.6
    });

    vg("border", "rect", {
        x:-HDW,
        y: HDH-10,
        width: DW,
        height:10,
        fill:bg,
        "fill-opacity":0.6
    });
    vg("border", "rect", {
        x:-HDW,
        y:-HDH,
        width: 10,
        height:DH,
        fill:bg,
        "fill-opacity":0.6
    });

    vg("border", "rect", {
        x:HDW-10,
        y: -HDH,
        width: 10,
        height:DH,
        fill:bg,
        "fill-opacity":0.6
    });
    return features;

}

